﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class ShipContainer {

	//The longitude and lattitude of the first ship
	//Every subsiquent ship's position in the virtual world is calculated relative to this
	public double centerLongitude { get; set;}
	public double centerLatitude { get; set;}

	//A list of all ships in this scene
	public List<Ship> shipList = new List<Ship>();

	public ShipContainer(double centerLongitude, double centerLatitude)
	{
		this.centerLongitude = centerLongitude;
		this.centerLatitude = centerLatitude;
	}
	//Adds a ship to shipList
	public void addShip(Ship ship)
	{
		shipList.Add(ship);
	}
	//Finds and returns a ship within the shipList list
	//This is done by comparing the mmsi with the AISMessage that is passed through
	//with the mmsi of ships within the shiplist
	public Ship findShip(DynamicAISMessage msg)
	{
		if(shipList != null && shipList.Any())
			foreach (Ship p in shipList) {
				if (p.MMSI.Equals(msg.MMSI))
					return p;
			}
		return null;
	}
}
