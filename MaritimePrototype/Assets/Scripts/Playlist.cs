﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Playlist {

	public string idplaylist;
	public string name;
	//a list that contains playlist items that are relavent to this instance of a playlist
	public List<PlaylistItem> playlistItemListLocal;

	// a hashtable that contains scenarios that are relavent to every playlist
	public static Hashtable scenarioHashtableGlobal;
	//a list the contains playlist items that are relevant to ever playlist
	public static List<PlaylistItem> playlistItemListGlobal;

	public Playlist(string idplaylist, string name)
	{
		this.idplaylist = idplaylist;
		this.name = name;
		playlistItemListLocal = new List<PlaylistItem> ();
	}

	public string toString()
	{
		return "ID: " + idplaylist + "\nName: " + name;
	}		
	//loads playlists from AIS server
	public void LoadPlaylistItems()
	{
		playlistItemListLocal = new List<PlaylistItem> ();
		//if playlist items global list doesnt exist then create it
		if (playlistItemListGlobal == null) {
			playlistItemListGlobal = ClientAIS.getPlaylistItems ();
		}
		//fill the playlist local with information that has been cut from playlist global
		foreach (PlaylistItem p in playlistItemListGlobal) {
			if (p.idplaylist.Equals(this.idplaylist))
				playlistItemListLocal.Add (p);
		}
	}
	//loads scenarios from AIS server
	public void LoadScenariosFromServer()
	{
		//if hash table of scenarios == null create it
		if (scenarioHashtableGlobal == null) {
			scenarioHashtableGlobal = ClientAIS.getScenarios ();
		}
		//gor through the playlist's local playlist items list
		foreach (PlaylistItem p in playlistItemListLocal) {
			//search for a scenario that contains this id
			if(scenarioHashtableGlobal .Contains(p.idscenario)){
				//add this scenario to the playlist item
				p.scenario = (Scenario)scenarioHashtableGlobal [p.idscenario];
			}
		}
	}
}

public class PlaylistItem{
	public string idplaylist_item;
	public string idplaylist;
	public string idscenario;
	public string idnext;
	public string idprev;
	public string index;
	public Scenario scenario;

	public PlaylistItem(string idplaylist_item, string idplaylist, string idscenario, string idnext, string idprev, string index)
	{
		this.idplaylist_item = idplaylist_item;
		this.idplaylist = idplaylist;
		this.idscenario = idscenario;
		this.idnext = idnext;
		this.idprev = idprev;
		this.index = index;
	}
}

public class Scenario{
	public string idscenario;
	public string idsession;
	public string time_start;
	public string time_end;
	public string area;
	public Double lat1;
	public Double long1;
	public Double lat2;
	public Double long2;
	public string name;

	public DynamicAISMessage[] messageList;

	public Scenario(string idscenario, string idsession, string time_start, string time_end, string area, Double lat1, Double long1, Double lat2, Double long2, string name)
	{
		this.idscenario = idscenario;
		this.idsession = idsession;
		this.time_start = time_start;
		this.time_end = time_end;
		this.area = area;
		this.lat1 = lat1;
		this.long1= long1;
		this.lat2 = lat2;
		this.long2 = long2;
		this.name = name;
	}

	//Creates a list of ais messages that conform to the specifications of the scenario
	public bool createMessageList()
	{
		if (messageList == null) {
			long unixTimestampStart = 0, unixTimestampEnd = 0;

			if (time_start != null) {
				// Convert start tim and end time to datetime objects
				char[] delimiterChars = { ' ', ',', '.', ':', '-', '\t' };

				//split strings
				String[] time_start_M = time_start.Split (delimiterChars);
				;
				String[] time_end_M = time_end.Split (delimiterChars);
				;

				//instantiate
				DateTime since = new DateTime (Int32.Parse (time_start_M [0]), Int32.Parse (time_start_M [1]), Int32.Parse (time_start_M [2]), Int32.Parse (time_start_M [3]), Int32.Parse (time_start_M [4]), Int32.Parse (time_start_M [5]));
				DateTime till = new DateTime (Int32.Parse (time_end_M [0]), Int32.Parse (time_end_M [1]), Int32.Parse (time_end_M [2]), Int32.Parse (time_end_M [3]), Int32.Parse (time_end_M [4]), Int32.Parse (time_end_M [5]));

				//get unixtimestamp
				unixTimestampStart = (long)(since.Subtract (new DateTime (1970, 1, 1))).TotalSeconds;
				unixTimestampEnd = (long)(till.Subtract (new DateTime (1970, 1, 1))).TotalSeconds;

			}
			/*Debug.Log ("scenario: "+idscenario);
		Debug.Log ("session: "+idsession);
		Debug.Log ("lat1: "+lat1);
		Debug.Log ("long1: "+long1);
		Debug.Log ("lat2: "+lat2);
		Debug.Log ("long2: "+long2);
		Debug.Log ("since: "+unixTimestampStart.ToString());
		Debug.Log ("since: "+time_start);
		Debug.Log ("till: "+unixTimestampEnd.ToString());
		Debug.Log ("till: "+time_end);*/

			try {
				//retrieve dynamic msgs from server
				messageList = ClientAIS.getAISSession (idsession, lat1, long1, lat2, long2, unixTimestampStart, unixTimestampEnd);
				//return bool to indicate success
				return true;
			} catch (NullReferenceException e) {
				Driver.msgError = "Couldn't load scenario";
				return false;
			}
		}
		return true;
	}

}
