﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Driver : MonoBehaviour {


	//Current gui message
	public static DynamicAISMessage msgGlobal;
	//For error messages
	public static string msgError;

	//Prefabs
	public GameObject AISMarker;
	public GameObject shipPrefab;
	//Containers
	public static ShipContainer shipContainer;
	public static DynamicAISMessage [] msgListGlobal;
	//Are we following a ship with our camera?
	public bool following;
	//The scroll position of the main camer
	public int scrollFollow;
	public int scrollGlobal;
	public static Playlist playlistDriver;
	public static PlaylistItem playlistItemChosen;

	public Text msgGlobalText;
	public Text msgErrorText;
	public GameObject loadingMSG;

	public Coroutine lastCoRoutine;

	//the speed at which sips move
	public float animationSpeed;

	// Use this for initialization
	IEnumerator Start () {
		msgGlobal = null;
		msgError = null;
		//initiateAISInformationSession();
		//createShip(msgList.ElementAt (0).longitude, msgList.ElementAt (0).latitude, msgList.ElementAt(0).trueheading, gameObjectShip);
		//'Yeild return ' to wait until everything is loaded
		yield return new WaitUntil(() => Camera.main != null);
		yield return new WaitUntil(() => AISMarker != null);
		yield return new WaitUntil(() => shipPrefab != null);
		yield return new WaitUntil(() => msgGlobalText != null);
		yield return new WaitUntil(() => msgErrorText != null);

		//Set initial camera position
		Camera.main.transform.Rotate (Vector3.right, 90);
		initiateAISInformationSession ();
	}
	
	// Update is called once per frame
	void Update () {
		/*scrollControl();
		cameraControl();*/
		//if there is a message to display (msgglobal does not equal null)
		if (msgGlobal != null)
			msgGlobalText.text = msgGlobal.ToStringShort ();
		//if there is none, set set string to null
		else
			msgGlobalText.text = "";

		//set error message
		msgErrorText.text = msgError;
		//android back button functionality
		if (Input.GetKeyDown (KeyCode.Escape)) {
			LoadPlaylistViewBehaviour.playlistLoadPlaylistView = playlistDriver;
			SceneManager.LoadSceneAsync("LoadPlaylistView");
		}
	}

	public void initiateAISInformationSession()
	{
		//if the last coroutine is still running stop it
		if (lastCoRoutine != null)
		{
			StopCoroutine(lastCoRoutine);
		}
		//delete all information related to a session
	    deleteSession();
		/*foreach (AISMessage p in playlistItemChosen.scenario.messageList) {
			UnityEngine.Debug.Log (p);
		}*/
		//load the scenario and initiate the scene (coroutine)
		try {
			playlistItemChosen.scenario.createMessageList();
			loadingMSG.SetActive (false);
			msgListGlobal = playlistItemChosen.scenario.messageList;
			lastCoRoutine = StartCoroutine (initiateScene ());
		}catch(NullReferenceException e){
			msgError = "Error loading scenario";
		}
	}

	//A method for creating a ship object
	public bool createShip(DynamicAISMessage msg)
	{
		//if a ship with this mmsi (mmsi within the aismessage object) exists return false
		if (shipContainer.findShip(msg) == null)
		{
			GameObject tempGameObjectShip = Instantiate(shipPrefab);
			Ship tempShip = new Ship(msg, tempGameObjectShip);
			shipContainer.addShip(tempShip);

			double leftRight = shipContainer.centerLongitude - msg.longitude;
			double backForward = shipContainer.centerLatitude - msg.latitude;

			
			//Debug.Log ("Ship created : " + tempShip.MMSI+ ", and distance is : " + cameraDistance);

			//tempGameObjectShip.transform.Rotate(Vector3.left * 90);
			return true;
		}
		else
		{
			return false;
		}
	}	
	//A method for creating the ship container
	public void createShipContainer()
	{
		try
		{
			//Create Ship Container
			shipContainer = new ShipContainer(msgListGlobal[0].longitude, msgListGlobal[0].latitude);
		}
		catch (ArgumentOutOfRangeException e)
		{
			//if ship container cant be created
			msgError = "No results returned";
		}
		catch (ArgumentNullException e)
		{
			//if ship container cant be created
			msgError = "No results returned";
		}
	}

	/*All redering methods used to visualize information within a session
	 * ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	*/
	//A method that sets the positions for 3D AISMarker objects and 3D Ship Objects
	IEnumerator initiateScene()
	{
		//Play bar cursor percentage made into an index representing the percentage relative to the leng of msgListGlobal
		float indexDriver = 0;
		createShipContainer();
		//Transform all AISMarkers
		for (int i = 0; i < msgListGlobal.Count(); i++)
		{
			//if a ship was not created, createShip returns false and we do not wait 0.4 seconds
			//this prevents the program waiting in-between creating ships as opposed to waiting in-between moving ships
			DynamicAISMessage tempMsg = msgListGlobal[i];
			createShip (tempMsg);
			Ship tempShip = shipContainer.findShip(tempMsg);
			tempShip.addMsg(tempMsg);
			transformAISMessage(tempShip, tempMsg);
			//Ship position
			//transformShip (p);
		}
		//Transform all Ships
		for (int i = 0; i < msgListGlobal.Count(); i++)
		{
			//pause functionality
			if (PlayBarBehaviour.pause == true) {
				yield return new WaitUntil (() => PlayBarBehaviour.pause == false);
			}
			//Check if play bar cursor has changed
			if (PlayBarBehaviour.change == true) {
				indexDriver = (int)(msgListGlobal.Count () * PlayBarBehaviour.indexPlayBarPercentage);
				i = 0;
				PlayBarBehaviour.change = false;
			}
			//Call to playbar behaviour
			GameObject.Find("Main Camera").GetComponent<PlayBarBehaviour>().updatePlayBar(i, msgListGlobal.Count());
			DynamicAISMessage tempMsg = msgListGlobal[i];

			//this prevents the program waiting in-between creating ships as opposed to waiting in-between moving ships
			//speed of ship divided by shiplist.count to maintain consistancy
			if(i > indexDriver)
				yield return new WaitForSeconds(animationSpeed / (float)shipContainer.shipList.Count());
			Ship tempShip = shipContainer.findShip(tempMsg);
			transformShip(tempShip, tempMsg);
			//Ship position
			//transformShip (p);
			//Makes sure that the for loop does not terminate on completion
			if (i == (msgListGlobal.Count () - 1)) {
				yield return new WaitUntil (() => PlayBarBehaviour.change == true);
				indexDriver = (int)(msgListGlobal.Count () * PlayBarBehaviour.indexPlayBarPercentage);
				i = 0;
				PlayBarBehaviour.change = false;
			}
		}
	}
	//A method for setting the position of a 3D ship object
	public void transformShip(Ship ship, DynamicAISMessage msg)
	{
		//Set trueheading back to original position if we are starting the scene from the beggining
		if (ship.currentDynamicMsg != null)
			ship.shipPrefab.transform.Rotate (Vector3.up, -ship.currentDynamicMsg.true_heading);

		//Set the current dynamic message (message to be transformed)
		ship.currentDynamicMsgChange(msg);

		//Calculates the ship position relative to the original position of the first AIS message in msgListGlobal
		double leftRight = shipContainer.centerLongitude - ship.currentDynamicMsg.longitude;
		double backForward = shipContainer.centerLatitude - ship.currentDynamicMsg.latitude;
			
		//Debug.Log ("idmessage : " + msg.idmessage + ", MMSI : " + msg.MMSI + ", leftRight : " + leftRight + ", backForward : " + backForward + ", TrueHeading: " + msg.trueheading);


		ship.shipPrefab.transform.position = new Vector3((float)leftRight * 1000, 0, (float)backForward * 1000);
		//Set trueheading
		ship.shipPrefab.transform.Rotate (Vector3.up, ship.currentDynamicMsg.true_heading);
	}
	//A method for setting the position of a 3D AISMarker object
	public void transformAISMessage(Ship ship, DynamicAISMessage msg)
	{
		GameObject tempAISMarker = Instantiate (AISMarker);
		tempAISMarker.SetActive (ship.showAisMarkers);
		//Calculates the marker position relative to the original position of the first AIS message in msgListGlobal
		double leftRight = shipContainer.centerLongitude - msg.longitude;
		double backForward = shipContainer.centerLatitude - msg.latitude;

		//Set aismessage position on screen
		tempAISMarker.transform.position = new Vector3 ((float)leftRight * 1000, 0, (float)backForward * 1000);
		msg.AISMarker = tempAISMarker;
	}
	/*
	* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

	//A method for stopping a session
	public void deleteSession()
	{
		//destroys all information relating to a ship container
		if (shipContainer != null)
		{
			UnityEngine.Debug.Log("Dumping stuff");
			Ship tempShip = null;
			DynamicAISMessage tempMessage = null;
			for (int i = 0; i<shipContainer.shipList.Count(); i++)
			{
				tempShip = shipContainer.shipList.ElementAt(i);
				for (int j = 0; j<tempShip.msgList.Count(); j++)
				{
					tempMessage = tempShip.msgList.ElementAt(j);
					Destroy(tempMessage.AISMarker);
				}
				Destroy(tempShip.shipPrefab);
				tempShip.msgList = null;
			}
			shipContainer.shipList.Clear();
			shipContainer = null;
		}
	}

	/*Camera controlls
	----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//A method for allowing scroll functionality on the Main Camera*/
	/*public void cameraControl()
	{
		//if input key == space then we check if we are following a ship
		if (Input.GetKeyDown("space"))
		{
			//if there is a ship to follow and we haven't started following a ship yet (we are initiating the follow procedure)
			if (ShipBehaviour.follow != null && following == false)
			{
				Camera.main.transform.position = new Vector3(ShipBehaviour.follow.transform.position.x, scrollFollow, ShipBehaviour.follow.transform.position.z);
				//we are now in follow mode
				following = true;
			}
			else
				//if we are not initiating follow then obviously we keep camera centered
			{
				Camera.main.transform.position = new Vector3(0, scrollGlobal, 0);
				following = false;
			}
		}
		//if we are currently within the procedure of following a ship we set the camera to the position of the ship
		else if (ShipBehaviour.follow != null && following == true)
		{
			Camera.main.transform.position = new Vector3(ShipBehaviour.follow.transform.position.x, scrollFollow, ShipBehaviour.follow.transform.position.z);
		}
		//other wise we set camera to center
		else
			Camera.main.transform.position = new Vector3(0, scrollGlobal, 0);	}

	public void scrollControl ()
	{
		//if we are following a ship we set the scollFollow variable when we recieve information from the middle mouse
		if (ShipBehaviour.follow != null && following == true)
		{
			//if the mouse scroll wheel has changed then save this information
			var d = Input.GetAxis("Mouse ScrollWheel");
			if (d > 0f)
			{
				scrollFollow -= 5;
			}
			else if (d < 0f)
			{
				scrollFollow += 5;
			}
			//if we are not following a ship then we get mouse scroll information and change scrollGlobal
		}else
		{
			var d = Input.GetAxis("Mouse ScrollWheel");
			if (d > 0f)
			{
				scrollGlobal -= 5;
			}
			else if (d < 0f)
			{
				scrollGlobal += 5;
			}
		}
	}*/
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/*All GUI
	 *----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	*/
	void OnGUI()
	{
		/*RectTransform rt = (RectTransform)GameObject.Find ("PlayBarCanvas").transform;
		if(msgGlobal != null)
			GUI.Label(new Rect(10, rt.rect.height, 500, 300), "" + msgGlobal.ToString());*/
		GUI.Label(new Rect(10, Screen.height - 20, 500, 300), "" + msgError);
		/*//GUI.Button (new Rect (Screen.width / Screen.width, Screen.height-20, 20, 20), "<-");
		//GUI.Button (new Rect ((Screen.width / Screen.width)+20, Screen.height-20, 20, 20), "->");*/
	}
}
