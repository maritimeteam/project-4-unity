﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadPlaylistViewBehaviour : MonoBehaviour {

	//text GUI to be displayed on screen
	public Text playListName;
	public Text playlistInformation;

	//Errror message to be displayed
	private string msgError = "";
	//The playlist that has been chosen in SearchView Scene within SearchBehaviour script
	public static Playlist playlistLoadPlaylistView;

	IEnumerator Start()
	{
		// we pass the chosen playlist from searchbehaviour script to loadplaylistview (current) script
		playlistLoadPlaylistView = SearchBehaviour.playlistSearchBehaviour;
		//initiate playlist information strings that will be loaded onto the sceen
		String nameString = playlistLoadPlaylistView.name;
		String playlistInfoString = playlistLoadPlaylistView.toString ();

		//Wait for variables to Load in the new scene
		yield return new WaitUntil(() => playListName != null);
		yield return new WaitUntil(() => playlistInformation != null);

		//load playlist information into GUI
		playListName.text = nameString;
		playlistInformation.text = playlistInfoString;
	}
	public void Update()
	{
		//android back button functionality
		if (Input.GetKeyDown (KeyCode.Escape)) {
			SceneManager.LoadSceneAsync("SearchView");
		}
	}

	//method to load the next scene
	public void LoadVisualizationView()
	{
		//Call methods within the playlist class to fill the playlist with information from the pivot maritime AIS server
		//This does hhtp requests for playlist items, scenarios and sessions
		try{
			playlistLoadPlaylistView.LoadPlaylistItems ();
		}catch(Exception E) {
			playlistInformation.text = "Error loading playlist items";
		}
		try{
			playlistLoadPlaylistView.LoadScenariosFromServer();
		}catch(Exception E) {
			playlistInformation.text = "Error loading scenarios";
		}
		/*playlistLoadPlaylistView.playlistItemListLocal.ElementAt (0).scenario.createMessageList();

		//We pass our chosen playlist to the driver*/
		Driver.playlistDriver = playlistLoadPlaylistView;
		//We start with the first playlist and put it into the driver 'play' variable
		//this is the playlist that will be played
		Driver.playlistItemChosen = playlistLoadPlaylistView.playlistItemListLocal.ElementAt (0);
		SceneManager.LoadScene("VisualizationView");
	}

	void OnGUI()
	{
		GUI.Label(new Rect(10, 130, 500, 300), "" + msgError);
	}
}
