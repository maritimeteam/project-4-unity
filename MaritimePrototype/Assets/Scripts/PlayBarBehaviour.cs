﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayBarBehaviour : MonoBehaviour {

	//Cursor within the playbar (the slider that you use to switch the time of a scene)
	public GameObject cursor;
	//The new position of the cursor once it has been changed from its ol position
	private Vector3 newPosition;
	//These markers are used to calculate the length of the playbar
	public GameObject markerLeft;
	public GameObject markerRight;

	//the percentage that the cursur fills in the playbar, relative to the playbar's length
	public static float indexPlayBarPercentage;

	// A bool to indicate whether or not we have changed the position of the cursor
	public static bool change;
	// A bool to indicate whether or not we have paused
	public static bool pause;
	//The pause button
	public GameObject button;

	void Start()
	{
		pause = false;
		//save the initial position of the cursor
		newPosition = cursor.transform.position;
	}

	public void OnPointerDown()
	{
		//Get mouse input
		newPosition.x = Input.mousePosition.x;
		//transform the cursor based on this input
		cursor.transform.position = newPosition;
		//update the scene 
		updateScene ();
	}

	public void Pause()
	{
		if (pause == false) {
			pause = true;
			button.GetComponentInChildren<Text>().text = "Play";
		} else {
			pause = false;
			button.GetComponentInChildren<Text>().text = "Pause";
		}
	}
	//Method to find position of cursaor on playbar
	public void updatePlayBar(int index, int total)
	{
		if (index > 0) {
			float distance = markerRight.transform.position.x - markerLeft.transform.position.x;
			newPosition.x = (float)(((float)index/total) * (distance));
			newPosition.x += markerLeft.transform.position.x;
			cursor.transform.position = newPosition;
		}
	}
	//Method to match the position of cursur to the actual programs logical display of 3d objects
	public void updateScene()
	{
		// calculate cursor position relative to playbar size
		float distance = markerRight.transform.position.x - markerLeft.transform.position.x;
		float cursorDistance = cursor.transform.position.x - markerLeft.transform.position.x;
		float cusorPositionRelative = cursorDistance / distance;

		//save the percentage that the cursor fills within the playbar and set the change variable to true
		//this will indcate to the driver script that we have changed the position of the cursor
		indexPlayBarPercentage = cusorPositionRelative;
		change = true;
	}

	//Goes to next view and passes the playlist
	public void LoadSettings()
	{
		SettingBehaviour.playlistSettingsView  = Driver.playlistDriver;
		SceneManager.LoadSceneAsync("SettingsView");
	}
}
