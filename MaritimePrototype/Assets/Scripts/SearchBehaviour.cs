﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SearchBehaviour : MonoBehaviour {

	//The playlist that has been chosen to be accessed in the next scene
	public static Playlist playlistSearchBehaviour;
	//Search-bar text
	public Text textSearch;
	//List of all playlists retrieved from server
	private List<Playlist> playlistList = new List<Playlist> ();
	//a list of GUI buttons to be displayed to user
	//each represents a playlist
	private List<GameObject> buttonList =  new List<GameObject> ();

	//for error messages
	public static string msgError;

	//the content panel in the gui which is used to house buttons
	public GameObject content;
	//a varriable to house individual button objects
    public GameObject playlistButtonGameobject;
	//An integer to control the space between each button
	public int buttonSpacing;

	void Start()
	{
		//Hiding original prefab button
		playlistButtonGameobject.SetActive(false);
		//Retrive the playlist list from AIS server
		playlistList = ClientAIS.getPlaylists();
		try{
		for(int i = 0; i < playlistList.Count ; i++)
			{
				Playlist tempPlaylist = playlistList.ElementAt(i);
				//set playlist button text
				playlistButtonGameobject.GetComponentInChildren<Text>().text = tempPlaylist.name;

				GameObject tempButtonGameobject = Instantiate(playlistButtonGameobject);

				//add to button list
				buttonList.Add(tempButtonGameobject);
				//Make the gameobject a button in order to be able to add a listener
				Button tempButton = (Button)tempButtonGameobject.GetComponent<Button>();
				//add an listenerr to the button object
				tempButton.onClick.AddListener(() => LoadPlaylist(tempPlaylist));

				//Transformations
				tempButtonGameobject.transform.SetParent(content.transform);
				//set to active and set position
				tempButtonGameobject.SetActive(true);
			}
		}catch(Exception  e) {
			playlistButtonGameobject.SetActive(false);
			msgError = "Server has returned no values";
		}
	}
	//Search method
	public void HitSearchButton()
	{
		foreach (GameObject b in buttonList) {
			Destroy (b);
		}
		for(int i = 0; i < playlistList.Count ; i++)
		{
			Playlist tempPlaylist = playlistList.ElementAt(i);
			//of playlist matches the serach text
			if (tempPlaylist.name.ToLower().Contains (textSearch.text.ToLower())) {
				//set playlist button text
				playlistButtonGameobject.GetComponentInChildren<Text> ().text = tempPlaylist.name;

				GameObject tempButtonGameobject = Instantiate (playlistButtonGameobject);

				//add to button list
				buttonList.Add (tempButtonGameobject);
				//Make the gameobject a button in order to be able to add a listener
				Button tempButton = (Button)tempButtonGameobject.GetComponent<Button> ();
				//add an listenerr to the button object
				tempButton.onClick.AddListener (() => LoadPlaylist (tempPlaylist));

				//Transformations
				tempButtonGameobject.transform.SetParent (content.transform);
				//set to active and set position
				tempButtonGameobject.SetActive (true);
			}
		}
	}
	//Goes to next view and passes the playlist
	private void LoadPlaylist(Playlist playlist)
	{
		playlistSearchBehaviour = playlist;
		SceneManager.LoadScene("LoadPlaylistView");
	}

	void OnGUI()
	{
		GUI.contentColor = Color.black;
		GUI.Label(new Rect(10, 130, 500, 300), "" + msgError);
		//GUI.Button (new Rect (Screen.width / Screen.width, Screen.height-20, 20, 20), "<-");
		//GUI.Button (new Rect ((Screen.width / Screen.width)+20, Screen.height-20, 20, 20), "->");
	}
}
