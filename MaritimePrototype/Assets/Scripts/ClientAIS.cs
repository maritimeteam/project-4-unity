﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using LitJson;
using System.Runtime.Serialization;
using System.Net;

public static class ClientAIS{


	//This is a method for grabbing AIS session data based upon a session number
	public static DynamicAISMessage [] getAISSession(String session, double lat1, double long1, double lat2, double long2, long since, long till)
	{
		try
		{
			String url= "";
			//This is our http request
			if(lat1 == 0 && long1 == 0 && since != 0){
				url = @"http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=NK2dGY3o2P&session="+ session + "&since=" +since+"&till="+till+"&page=all";
				//Debug.Log("trigger 1");
			}else if(since == 0 && lat1 != 0 && long1 != 0){
				url = @"http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=NK2dGY3o2P&session="+ session +"&lat1="+lat1+"&long1="+long1+"&lat2="+lat2+"&long2="+long2+"&page=all";
				//Debug.Log("trigger 2");
			}else if(since == 0 && lat1 == 0 && long1 == 0){
				url = @"http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=NK2dGY3o2P&session="+ session +"&page=all";
				//Debug.Log("trigger 3");
			}else{
				url = @"http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=NK2dGY3o2P&session="+ session + "&since=" + since.ToString() +"&till="+ till.ToString() +"&lat1="+ lat1.ToString() +"&long1="+ long1.ToString() +"&lat2="+ lat2.ToString() +"&long2="+ long2.ToString()+"&page=all";
				//Debug.Log("trigger 4");
			}
			//We download the string and save it
			String json = new WebClient().DownloadString(url);
			//We decode the string and save it to a list of msg objects
			DynamicAISMessage [] msgList = decodeAISMessages(json);
			//we set the error to null to ensure that previous msgs from previous failed attempts are cleared
			Driver.msgError = "";
			return msgList;
		}
		catch (WebException e)
		{
			Driver.msgError = "Error retrieving AIS Session/Scenario";
		}
		return null;
	}

	//A method to return a list of unfiltered playlist items
	public static List<PlaylistItem> getPlaylistItems()
	{
		try
		{
			//This is our http request
			String url = @"http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/playlist_items.php?passwd=NK2dGY3o2P";
			//We download the string and save it
			String json = new WebClient().DownloadString(url);
			//We decode the string and save it to a list of playlist items objects
			List<PlaylistItem> playlistItemsList = decodePlaylistItems(json);
			//we set the error to null to ensure that previous msgs from previous failed attempts are cleared
			Driver.msgError = "";
			return playlistItemsList;
		}
		catch (WebException e)
		{
			Driver.msgError = "Invalid input sent to server";
		}
		return null;
	}

	//Queries playlists and returns exactly that. playlists
	public static List<Playlist> getPlaylists()
	{
		try
		{
			//This is our http request
			String url = @"http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/playlists.php?passwd=NK2dGY3o2P";
			//We download the string and save it
			String json = new WebClient().DownloadString(url);
			//We decode the string and save it to a list of playlist objects
			List<Playlist> playlistList = decodePlaylists(json);
			//we set the error to null to ensure that previous msgs from previous failed attempts are cleared
			Driver.msgError = "";
			return playlistList;
		}
		catch (WebException e)
		{
			Driver.msgError = "Invalid input sent to server";
		}
		return null;
	}

	//Queries the server and returnas a hashtable of scenarios
	public static Hashtable getScenarios()
	{
		try
		{
			//This is our http request
			String url = @"http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/scenarios.php?passwd=NK2dGY3o2P";
			//We download the string and save it
			String json = new WebClient().DownloadString(url);
			//We decode the string and save it to a hashtable of scenario objects
			Hashtable scenarioHashtable = decodeScenarios(json);
			//we set the error to null to ensure that previous msgs from previous failed attempts are cleared
			Driver.msgError = "";
			return scenarioHashtable;
		}
		catch (WebException e)
		{
			Driver.msgError = "Invalid input sent to server";
		}
		return null;
	}

	//DECODERS!!!!!!!!!
	//-------------------------------------------------------------------------------------------------------------

	//This is a method for turning a JSON string into an AISMessage Object
	//public static List<DynamicAISMessage> decodeAISMessages(String json)
	public static DynamicAISMessage [] decodeAISMessages(String json)
	{
		//int count = 0;
		List<DynamicAISMessage> msgList = new List<DynamicAISMessage>();
		List<String> rediculesArray = new List<String> ();
		/*String json = File.ReadAllText(Application.dataPath + "./ais_dynamic_messages_examples.json");*/


		DynamicAISMessage [] arrayDMG = JsonMapper.ToObject<DynamicAISMessage[]>(json);
		//msgList = arrayDMG.OfType<DynamicAISMessage>().ToList();
		/*//Creates an array of values from the json string
		JsonReader reader = new JsonReader(json);
		while (reader.Read()) {
			if(reader.Value is string)
				rediculesArray.Add ((string)reader.Value);
			else
				rediculesArray.Add (Convert.ToString(reader.Value));
		}

		//Trims the array of null characters
		for (int j = 0; j < 100; j=j+18) {
			rediculesArray.RemoveAt(j);
			rediculesArray.RemoveAt(j);
			if(j >= rediculesArray.Count ())break;
			for (int i = j; i < j + 18; i++){
				rediculesArray.RemoveAt(i);
			}
		}

		//iterates through the array and adds objects to the data structure
		for (int i = 0; i < 100; i+2) {
			msgList.Add(new DynamicAISMessage(rediculesArray.ElementAt(i++), rediculesArray.ElementAt(i++),  rediculesArray.ElementAt(i++), 
				rediculesArray.ElementAt(i++),  Byte.Parse(rediculesArray.ElementAt(i++)),  rediculesArray.ElementAt(i++),  Byte.Parse(rediculesArray.ElementAt(i++)), 
				Double.Parse(rediculesArray.ElementAt(i++)),  Double.Parse(rediculesArray.ElementAt(i++)),  Byte.Parse(rediculesArray.ElementAt(i++)),  Double.Parse(rediculesArray.ElementAt(i++)), Double.Parse(rediculesArray.ElementAt(i++)), 
				Double.Parse(rediculesArray.ElementAt(i++)), Int16.Parse(rediculesArray.ElementAt(i++)), Byte.Parse(rediculesArray.ElementAt(i++)), Byte.Parse(rediculesArray.ElementAt(i++)), Byte.Parse(rediculesArray.ElementAt(i++)),
				Byte.Parse(rediculesArray.ElementAt(i++))));
			Debug.Log ("Decode AIS Messages: " + count++);
		}
		/*for (int i = 0; i<rediculesArray.Count(); i=i) {
			Debug.Log("1" + rediculesArray.ElementAt(i++) + ", 2" + rediculesArray.ElementAt(i++)+", 3" +   rediculesArray.ElementAt(i++)+", 4" + 
				rediculesArray.ElementAt(i++)+", 5" +   Byte.Parse(rediculesArray.ElementAt(i++))+", 6" +   rediculesArray.ElementAt(i++)+", 7" +   Byte.Parse(rediculesArray.ElementAt(i++))+", " + 
				Double.Parse(rediculesArray.ElementAt(i++))+ ", " +  Double.Parse(rediculesArray.ElementAt(i++))+  ", " + Byte.Parse(rediculesArray.ElementAt(i++))+  ", " + Double.Parse(rediculesArray.ElementAt(i++))+ ", " + Double.Parse(rediculesArray.ElementAt(i++))+ ", " + 
			    Double.Parse(rediculesArray.ElementAt(i++))+", " +  Int16.Parse(rediculesArray.ElementAt(i++))+ ", " + Byte.Parse(rediculesArray.ElementAt(i++))+ ", " + Byte.Parse(rediculesArray.ElementAt(i++))+", " +  Byte.Parse(rediculesArray.ElementAt(i++))+", " + 
				Byte.Parse(rediculesArray.ElementAt(i++)));
		}*/



		return arrayDMG;
	}
	//Decodes scenarios within a playlist
	public static List<PlaylistItem> decodePlaylistItems(String json)
	{
		List<PlaylistItem> playlistItemsList = new List<PlaylistItem>();
		List<String> rediculesArray = new List<String> ();
		/*String json = File.ReadAllText(Application.dataPath + "./ais_dynamic_messages_examples.json");*/

		//Creates an array of values from the json string
		JsonReader reader = new JsonReader(json);
		while (reader.Read()) {
			if(reader.Value is string)
				rediculesArray.Add ((string)reader.Value);
			else
				rediculesArray.Add (Convert.ToString(reader.Value));
		}
		//Trims the array of null characters
		for (int j = 0; j < rediculesArray.Count (); j=j+6) {
			rediculesArray.RemoveAt(j);
			rediculesArray.RemoveAt(j);
			if(j >= rediculesArray.Count ())break;
			for (int i = j; i < j + 6; i++){
				rediculesArray.RemoveAt(i);
			}
		}
		//iterates through the array and adds objects to the data structure
		for (int i = 0; i < rediculesArray.Count(); i=i) {
			playlistItemsList.Add (new PlaylistItem (rediculesArray.ElementAt (i++), rediculesArray.ElementAt (i++), rediculesArray.ElementAt (i++), rediculesArray.ElementAt (i++), rediculesArray.ElementAt (i++), rediculesArray.ElementAt (i++)));
		}
		/*for (int i = 0; i<rediculesArray.Count(); i=i) {
			Debug.Log("1" + rediculesArray.ElementAt(i++) + ", 2" + rediculesArray.ElementAt(i++)+", 3" +   rediculesArray.ElementAt(i++)+", 4" + 
				rediculesArray.ElementAt(i++)+", 5" +   Byte.Parse(rediculesArray.ElementAt(i++))+", 6" +   rediculesArray.ElementAt(i++)+", 7" +   Byte.Parse(rediculesArray.ElementAt(i++))+", " + 
				Double.Parse(rediculesArray.ElementAt(i++))+ ", " +  Double.Parse(rediculesArray.ElementAt(i++))+  ", " + Byte.Parse(rediculesArray.ElementAt(i++))+  ", " + Double.Parse(rediculesArray.ElementAt(i++))+ ", " + Double.Parse(rediculesArray.ElementAt(i++))+ ", " + 
			    Double.Parse(rediculesArray.ElementAt(i++))+", " +  Int16.Parse(rediculesArray.ElementAt(i++))+ ", " + Byte.Parse(rediculesArray.ElementAt(i++))+ ", " + Byte.Parse(rediculesArray.ElementAt(i++))+", " +  Byte.Parse(rediculesArray.ElementAt(i++))+", " + 
				Byte.Parse(rediculesArray.ElementAt(i++)));
		}*/



		return playlistItemsList;
	}
	//Decodes scenarios
	public static Hashtable decodeScenarios(String json)
	{
		Hashtable scenarioHashtable = new Hashtable();
		List<String> rediculesArray = new List<String> ();
		/*String json = File.ReadAllText(Application.dataPath + "./ais_dynamic_messages_examples.json");*/

		//Creates an array of values from the json string
		JsonReader reader = new JsonReader(json);
		while (reader.Read()) {
			if(reader.Value is string)
				rediculesArray.Add ((string)reader.Value);
			else
				rediculesArray.Add (Convert.ToString(reader.Value));
		}
		//Trims the array of null characters
		for (int j = 0; j < rediculesArray.Count (); j=j+10) {
			rediculesArray.RemoveAt(j);
			rediculesArray.RemoveAt(j);
			if(j >= rediculesArray.Count ())break;
			for (int i = j; i < j + 10; i++){
				rediculesArray.RemoveAt(i);
			}
		}
		//iterates through the array and adds objects to the data structure
		for (int i = 0; i < rediculesArray.Count(); i=i) {
			scenarioHashtable.Add (rediculesArray.ElementAt(i),new Scenario(rediculesArray.ElementAt(i++), rediculesArray.ElementAt(i++), 
				rediculesArray.ElementAt(i++), rediculesArray.ElementAt(i++), rediculesArray.ElementAt(i++), rediculesArray.ElementAt(i++) == null ? Double.Parse(rediculesArray.ElementAt(i++)) : 0
				, rediculesArray.ElementAt(i++) == null ? Double.Parse(rediculesArray.ElementAt(i++)) : 0, rediculesArray.ElementAt(i++) == null ? Double.Parse(rediculesArray.ElementAt(i++)) : 0,
				rediculesArray.ElementAt(i++) == null ? Double.Parse(rediculesArray.ElementAt(i++)) : 0,rediculesArray.ElementAt(i++)));
		}

		return scenarioHashtable;
	}
	//Decode actual playlists
	public static List<Playlist> decodePlaylists(String json)
	{
		List<Playlist> playlistList = new List<Playlist>();
		List<String> rediculesArray = new List<String> ();
		/*String json = File.ReadAllText(Application.dataPath + "./ais_dynamic_messages_examples.json");*/

		//Creates an array of values from the json string
		JsonReader reader = new JsonReader(json);
		while (reader.Read()) {
			if(reader.Value is string)
				rediculesArray.Add ((string)reader.Value);
			else
				rediculesArray.Add (Convert.ToString(reader.Value));
		}
		//Trims the array of null characters
		for (int j = 0; j < rediculesArray.Count (); j=j+2) {
			rediculesArray.RemoveAt(j);
			rediculesArray.RemoveAt(j);
			if(j >= rediculesArray.Count ())break;
			for (int i = j; i < j + 2; i++){
				rediculesArray.RemoveAt(i);
			}
		}
		//iterates through the array and adds objects to the data structure
		for (int i = 0; i < rediculesArray.Count(); i=i) {
			playlistList.Add (new Playlist(rediculesArray.ElementAt(i++), rediculesArray.ElementAt(i++)));
		}
			
		return playlistList;
	}
	/*
     * PW: NK2dGY3o2P
     * http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=teampassword&session=316
     * http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=teampassword&mmsi=4575755
     * homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=NK2dGY3o2P&session=374&since=10453008&till=10451103&lat1=-27.2516653458&long1=153.32435285852&lat2=-27.2336986542&long2=153.34456114148&page=all
     * http://homepage.cs.latrobe.edu.au/16maritime01/api/v5/ais_voyages_dynamic.php?passwd=NK2dGY3o2P&session=314&since=1479132206&till=1479136597&page=all
	*/
}
