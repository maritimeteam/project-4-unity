﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	//Movement variables----------------------------------------------
	private Touch initTouch = new Touch();
	private float posX = 0f;
	private float posZ = 0f;
	private float preX = 0f;
	private float preZ = 0f;
	private Vector3 originPos;
	public float speed = 0.075f;
	public float speedMultiplier = 0.015f;
	//Scroll varaibles--------------------------------------------
	public float scrollSpeedMultiplier = 0.03f;
	public float scrollSpeed = 0.15f;
	public float scroll = 25f;

	public GameObject vizualizationCanvas;
	public GameObject vizualizationPanel;

	// Use this for initialization
	void Start () {
		//set the initial camera position
		Camera.main.transform.position = new Vector3(0, scroll, 0);
		//set the originpos variable to equal the main camera position
		originPos = Camera.main.transform.position;
		//set the posX and posZ we are using, to equal the original posX and posZ
		posX = originPos.x;
		posZ = originPos.z;
	}

	// Update is called once per frame
	void FixedUpdate () {
		RectTransform rectCanvas = vizualizationCanvas.GetComponent<RectTransform> ();
		RectTransform rectPanel = vizualizationPanel.GetComponent<RectTransform> ();
		float offset = Screen.height - (rectPanel.rect.height*2);
		//If the user is placing two thumbs on the screen at once
		//The device will register 2 touches
		if (Input.touchCount == 2) {
			//This 'if' statement makes sure that the 'touch' is beneath the play bar panel
			if (Input.GetTouch (0).position.y < offset && Input.GetTouch (1).position.y < offset) {
				//Setting touch variables representing touch zero and one
				Touch touchZero = Input.GetTouch (0);
				Touch touchOne = Input.GetTouch (1);

				// We then minus the touchPOsition (start of touch) with the current touch position
				//Aka deltaPosition. This gives us the distance we have moved from the start of the touch
				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

				// by subtracting one touch by the other touch we can calculate the distance between touches
				//This calculates the distance each touch has moved relative to each other
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;//Magnitude is the distance of the length of a vector (pretty much its' length)
				//This calculates the difference in distance each touch is currently at
				float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

				//This minus's the current distance (touchDeltaMag) form the distance that has been moved (prevTouchDeltaMag)
				//I have no idea WHY we are doing this i just copied this from online
				float deltaMagnitudediff = prevTouchDeltaMag - touchDeltaMag;

				//Then we add to the scroll varriable based on a complicated equation the we will hopefully never need to change
				scroll += (deltaMagnitudediff * (scrollSpeed * (scroll * scrollSpeedMultiplier)));
				//Scroll cannot be less than Zero - this line makes that so
				if (scroll < 0)
					scroll = 0;
			//scroll cannot be greater than 900 - this line makes it so
			else if (scroll > 900)
					scroll = 900;
				//Set the scroll to the camera (posX and posZ have not been changewd but are required for the method signature)
				Camera.main.transform.position = new Vector3 (posX, scroll, posZ);
		}
			//if theere is only one touch registerd on the device screen
			//then we are moving the camera and not zooming
		} else if (Input.touchCount == 1) {
			//This 'if' statement makes sure that the 'touch' is beneath the play bar panel
			if (Input.GetTouch (0).position.y < offset) {
				//A touch object holds an array of touches that represent touches along the distance of the screen that was touched
				//Input.touches is an array of touches within a touch object
				foreach (Touch touch in Input.touches) {
					//if we are JUST STARTING  the touch then assign initial values
					if (touch.phase == TouchPhase.Began) {
						//We have our fresh Touch object
						initTouch = touch;
						//we assign the initial positions of this fresh touch
						//We call preZ by that name because the third position in a Vector object's constructor is called Z
						//this is confusing as it is actually called Y in the position varriable of initTouch
						preX = initTouch.position.x;
						preZ = initTouch.position.y;
						//Error message should be set to null once we have started moving agian
						Driver.msgError = "";
					}
			//if the touch has been registered as moving across the screen
			else if (touch.phase == TouchPhase.Moved) {
						//we check to see how far the touch has moved and put this information within the delta X and Z variables
						float deltaX = preX - touch.position.x;
						float deltaZ = preZ - touch.position.y;
						//IF the camera if within its' appropriate boundaries then let it move
						if (Camera.main.transform.position.x < 1000 && Camera.main.transform.position.z < 1000 && Camera.main.transform.position.x > -1000 && Camera.main.transform.position.z > -1000) {
							//This will prevent the camera from moving sperratically 
							if ((preX - touch.position.x) < 75 && (touch.position.x - preX) < 75 && (preZ - touch.position.y) < 75 && (touch.position.y - preZ) < 75) {
								//The positions that we want to change are calculated
								//there is a speedmultiplier for adjusting te scroll based on how far the camera is off the water
								posX += deltaX * speed * (scroll * speedMultiplier);
								posZ += deltaZ * speed * (scroll * speedMultiplier);
								//Now set the camera to the appropraitte position
								Camera.main.transform.position = new Vector3 (posX, scroll, posZ);
							} 
							//reveret the variables to equal the current touch position
							//if we did not do this the deltaX distance and DeltaZ would grow exponentialy
							preX = touch.position.x;
							preZ = touch.position.y;
						} else { //ELSE place it back to the scenter of the scene
							posX = 0;
							posZ = 0;
							Camera.main.transform.position = new Vector3 (posX, scroll, posZ);
							Driver.msgError = "You have reached the edge of the map";
						}
					}
			//once the touch has ended, effectively destroy the touch object
			else if (touch.phase == TouchPhase.Ended) {
						initTouch = new Touch ();
					}
				}
			}
		}
	}
}

