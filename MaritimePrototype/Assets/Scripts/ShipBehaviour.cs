﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehaviour : MonoBehaviour {

	//Gamebject to follow
	public Ship thisShip;
	public Color startcolor;
	public static GameObject follow;

	//This method finds the ship object related to the ship gameobject
	//that this script is attached to, and fills the 'thisShip' variable
	void CreateConnection()
	{
		//Go through shipList within shipContainer and retrive the ship that 
		//matches the gameobject that this script is attached to
		foreach (Ship p in Driver.shipContainer.shipList) {
			if (this.gameObject == p.shipPrefab) {
				thisShip = p;
			}
		}
		//StartCoroutine(this.gameObject.GetComponentInChildren<GoogleMap>().Grab());
	}
	// this object was clicked - do something
	void OnMouseUp()
	{
		CreateConnection ();
		// Toggle all AISMarkers related to the gameObject to show
		showData ();
		//get the AIS message related to the gameObject
		getMessage ();
		//Tells the program that an AIS message marker is not being displayed
		AISMarkerBehaviour.AISMarkerOn = false;

	}
	//Gets the current AIS message for the ship that has been clicked and changes MSGGlobal
	public void getMessage()
	{
		if (Driver.msgGlobal != null && Driver.msgGlobal.ToString() == thisShip.currentDynamicMsg.ToString ()) {
			Driver.msgGlobal = null;
		}else
			Driver.msgGlobal = thisShip.currentDynamicMsg;

	}
	//Updates MSGlobal to match the AIS string of the ship currently being followed
	//This occurs every time the position of the ship being followed is updated
	public void updateMessage()
	{
			Driver.msgGlobal = thisShip.currentDynamicMsg;
	}
	//This is the method that toggles the 3D AISMarker objects, belonging to a ship, to either 'show' or 'hidden'
	public void showData()
	{	
		thisShip.toggleShowAisMarkers ();
		//Goes through all the msgs within this ship object and turns the AISMarkers to show
		for (int i=0; i < (thisShip.msgList.Count-3); i=i+4) {
			if (thisShip.msgList[i].AISMarker != null) {
				thisShip.msgList[i].AISMarker.SetActive (thisShip.showAisMarkers);
				}
			}
	}
}
