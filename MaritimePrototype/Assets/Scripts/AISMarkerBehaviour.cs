﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISMarkerBehaviour : MonoBehaviour {

	public static bool AISMarkerOn = false;

	void OnMouseUp()
	{
		// this object was clicked - do something and pass through this gameobject 
		//(the gameobject this script is attached to)
		showAISMessage(this.gameObject);
		//Tells the program that an AIS message marker is not being displayed
		if (AISMarkerOn == false)
			AISMarkerOn = true;
		else
			AISMarkerOn = false;
	}

	//Shows the AISMessage that belongs to the 3D ais marker that has been clicked
	public void showAISMessage(GameObject marker)
	{
		//For every ship inside of our ship container. Iterate through list each message list within each ship
		foreach (Ship p in Driver.shipContainer.shipList) {
			foreach (DynamicAISMessage msg in p.msgList) {
				//if the gameobject that this script belongs to equals a gameobject within a ship list's msg list
				//show the AISMessage (change driver.msgGlobal)
				if (marker == msg.AISMarker) {
					//If the global msg already equals this message then make it turn off the message
					if (Driver.msgGlobal != null && msg == Driver.msgGlobal)
						Driver.msgGlobal = null;
					//else turn the messsage on (print to screen)
					else
						Driver.msgGlobal = msg;
				}
			}
		}
	}
}