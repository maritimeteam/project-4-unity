﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DynamicAISMessage {

	public int idmessage { get; set;}
	public int idsession { get; set;}
	public string time_stamp_system { get; set;}
	public string NMEA_string { get; set;}
	public int processed { get; set;}
	public int MMSI { get; set;}
	public int navigation_status { get; set;}
	public double ROT { get; set;}
	public double SOG { get; set;}
	public int position_accuracy { get; set;}
	public double longitude { get; set;}
	public double latitude { get; set;}
	public double COG { get; set;}
	public int true_heading { get; set;}
	public int maneuver_indicator { get; set;}
	public int RAIM_flag { get; set;}
	public int diagnostic_information { get; set;}
	public int time_stamp_seconds_only { get; set;}
	public GameObject AISMarker { get; set;}

	public DynamicAISMessage()
	{}

	public DynamicAISMessage(int idmessage, int idsession, string time_stamp_system, string NMEA_string, 
		int processed, int MMSI, int navigation_status, double ROT, double SOG, int position_accuracy, double longitude, double latitude, double COG,  
		int trueheading, int maneuver_indicator, int RAIM_flag, int diagnostic_information, int time_stamp_seconds_only)
	{
		this.idmessage = idmessage;
		this.idsession = idsession;
		this.time_stamp_system = time_stamp_system;
		this.NMEA_string = NMEA_string;
		this.processed = processed;
		this.MMSI = MMSI;
		this.navigation_status = navigation_status;
		this.ROT = ROT;
		this.SOG = SOG;
		this.position_accuracy = position_accuracy;
		this.longitude = longitude;
		this.latitude = latitude;
		this.COG = COG;
		this.true_heading = trueheading;
		this.maneuver_indicator = maneuver_indicator;
		this.RAIM_flag = RAIM_flag;
		this.diagnostic_information = diagnostic_information;
		this.time_stamp_seconds_only = time_stamp_seconds_only;
	}

	public string ToString()
	{
		return "idmessage : " + idmessage+
			"\nidsession : " + idsession+
			"\ntime_stamp_system : " + time_stamp_system+
			"\nNMEA_string : " + NMEA_string+
			"\nprocessed : " + processed+
			"\nMMSI : " + MMSI+
			"\nnavigation_status : " + navigation_status+
			"\nROT : " + ROT+
			"\nSOG : " + SOG+
			"\nposition_accuracy : " + position_accuracy+
			"\nlongitude : " + longitude+
			"\nlatitude : " + latitude+
			"\nCOG : " + COG+
			"\ntrueheading : " + true_heading+
			"\nmaneuver_indicator : " + maneuver_indicator+
			"\ndiagnostic_information : " + diagnostic_information+
			"\ntime_stamp_seconds_only : " + time_stamp_seconds_only;
	}

	public string ToStringShort()
	{
		return "Dynamic AIS Message"+
			"\nidmessage : " + idmessage+
			"\ntime_stamp_system : " + time_stamp_system+
			"\nMMSI : " + MMSI+
			"\nlongitude : " + longitude+
			"\nlatitude : " + latitude+
			"\ntrueheading : " + true_heading;
	}
}
	