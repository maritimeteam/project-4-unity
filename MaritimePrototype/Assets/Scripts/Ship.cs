﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship{

	public int MMSI { get; set;}
	//Decides if we should show the AIS markers inside of the AIS messages within the msglist
	public bool showAisMarkers{ get; set;}
	public GameObject shipPrefab { get; set;}
	// a list of AIS messages with this ships mmsi
	public List<DynamicAISMessage> msgList{get; set;}
	//the message that we are currently up to when playing through a scene in
	//in vizualisation view
	public DynamicAISMessage currentDynamicMsg;
	public StaticAISMessage staticMsg;

	public Ship ()
	{}

	public Ship (DynamicAISMessage msg, GameObject shipPrefab)
	{
        this.shipPrefab = shipPrefab;
		this.MMSI = msg.MMSI;
		msgList = new List<DynamicAISMessage> ();
		showAisMarkers = false;
		this.addMsg(msg);
	}
	//Adds an AIS message to the msgList if it does not already exist
	public void addMsg(DynamicAISMessage msg)
	{
		if (msgList.IndexOf (msg) == -1) {
			msgList.Add (msg);
		}
	}

	public string ToString()
	{
		return "idmessage = ";
	}

	// Sets the current dynamic message of this ship
	public void currentDynamicMsgChange(DynamicAISMessage msg){
		//if we are displaying this ship's dynamic AIS message data then we need to update that infol
		if (Driver.msgGlobal != null && currentDynamicMsg == Driver.msgGlobal && AISMarkerBehaviour.AISMarkerOn != true) {
			currentDynamicMsg = msg;
			this.shipPrefab.GetComponent<ShipBehaviour> ().updateMessage ();
		} 
		//other wise we need to update no info except the currentDynamicMsg
		else {
			currentDynamicMsg = msg;
		}
	}
	//This method sets the AIS markers within msgList AISMessage objects to show or to hide
	public void toggleShowAisMarkers ()
	{
		if (showAisMarkers == true && Driver.msgGlobal != null && Driver.msgGlobal.ToString() == this.currentDynamicMsg.ToString())
			showAisMarkers = false;
		else
			showAisMarkers = true;
	}
}
