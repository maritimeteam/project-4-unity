﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outline : MonoBehaviour {
	private Material material;
	private Color normalColor;
	private Color highlightColor;
	private static GameObject highlightedObject;

	void Start() {
		material = GetComponent<MeshRenderer>().material;

		normalColor = material.color;
		highlightColor = new Color(
			normalColor.r * 100f,
			normalColor.g * 0f,
			normalColor.b * 0f
		);
	}


	void OnMouseDown() {
		if(highlightedObject != null)
			highlightedObject.GetComponent<Outline>().revert();
		if (highlightedObject != this.gameObject) {
			material.color = highlightColor;
		}
		if (highlightedObject == this.gameObject) {
			highlightedObject = null;
		}else
			highlightedObject = this.gameObject;
	}

	public void revert()
	{
		material.color = normalColor;
	}
	
}
