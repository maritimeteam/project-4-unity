﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticAISMessage {

	private int idmessage{ get; set;}
	private int idsession{ get; set;}
	private string time_stamp_system{ get; set;}
	private string NMEA_string{ get; set;}
	private int processed{ get; set;}
	private int MMSI{ get; set;}
	private int AIS_version{ get; set;}
	private int IMO_number{ get; set;}
	private string call_sign{ get; set;}
	private string name{ get; set;}
	private int type_of_ship_and_cargo{ get; set;}
	private int bow_to_position_unit{ get; set;}
	private int stern_to_position_unit{ get; set;}
	private int port_to_position_unit{ get; set;}
	private int starboard_to_position_unit{ get; set;}
	private int type_of_position_fixing_divice{ get; set;}
	private string ETA{ get; set;}
	private string destination{ get; set;}
	private int last_static_draught{ get; set;}
	private int DTE{ get; set;}

	public StaticAISMessage()
	{}

	public StaticAISMessage(
		int idmessage,
		int idsession,
		string time_stamp_system,
		string NMEA_string,
		int processed,
		int MMSI,
		int AIS_version,
		int IMO_number,
		string call_sign,
		string name,
		int type_of_ship_and_cargo,
		int bow_to_position_unit,
		int stern_to_position_unit,
		int port_to_position_unit,
		int starboard_to_position_unit,
		int type_of_position_fixing_divice,
		string ETA,
		string destination,
		int last_static_draught,
		int DTE
	)
	{
		this.idmessage = idmessage;
		this.idsession = idsession;
		this.time_stamp_system = time_stamp_system;
		this.NMEA_string = NMEA_string;
		this.processed = processed;
		this.MMSI = MMSI;
		this.AIS_version = AIS_version;
		this.IMO_number = IMO_number;
		this.call_sign = call_sign;
		this.name = name;
		this.type_of_ship_and_cargo = type_of_ship_and_cargo;
		this.bow_to_position_unit = bow_to_position_unit;
		this.stern_to_position_unit = stern_to_position_unit;
		this.port_to_position_unit = port_to_position_unit;
		this.starboard_to_position_unit = starboard_to_position_unit;
		this.type_of_position_fixing_divice = type_of_position_fixing_divice;
		this.ETA = ETA;
		this.destination = destination;
		this.last_static_draught = last_static_draught;
		this.DTE = DTE;
	}
	public string ToString()
	{
		return "idmessage : " + idmessage +
		"\nidsession : " + idsession +
		"\ntime_stamp_system : " + time_stamp_system +
		"\nNMEA_string : " + NMEA_string +
		"\nprocessed : " + processed +
		"\nMMSI : " + MMSI +
		"\nAIS_version : " + AIS_version +
		"\nIMO_number : " + IMO_number +
		"\ncall_sign : " + call_sign +
		"\name : " + name +
		"\ntype_of_ship_and_cargo : " + type_of_ship_and_cargo +
		"\nbow_to_position_unit : " + bow_to_position_unit +
		"\nstern_to_position_unit : " + stern_to_position_unit +
		"\nport_to_position_unit : " + port_to_position_unit +
		"\nstarboard_to_position_unit : " + starboard_to_position_unit +
		"\ntype_of_position_fixing_divice : " + type_of_position_fixing_divice +
		"\nETA : " + ETA +
		"\ndestination : " + destination +
		"\nlast_static_draught : " + last_static_draught +
		"\nDTE : " + DTE;
	}
}
