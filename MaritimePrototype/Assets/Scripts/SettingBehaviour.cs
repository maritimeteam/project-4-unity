﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingBehaviour : MonoBehaviour {

	public static Playlist playlistSettingsView;
	//Dropdown object for playlists
	public Dropdown dropdown;
	//List of scenarios within a playlist
	private List<String> scenarioList = new List<String> ();
	public static PlaylistItem playlistItemChosen;

	//Thhe GUI for body and title
	public Text bodyText;
	public Text title;

	// Use this for initialization
	void Start () {
		// make the title == the chosen playlist name
		title.text = playlistSettingsView.name;
		//for each playlist item in plaaylist local within the chosen playlist
		//we add it's id to the scenario list of strings
		foreach(PlaylistItem p in playlistSettingsView.playlistItemListLocal){
			scenarioList.Add (p.idscenario);
		}
		//we clear the scenario dropdown list
		dropdown.ClearOptions ();
		try{
			//we add all our options to the dropdown from the scenario list of strings
			dropdown.AddOptions (scenarioList);
		}catch(NullReferenceException  e) {
			Driver.msgError = "Server has returned no values";
		}
		//initiate the chosen playlist variable
		playlistItemChosen = playlistSettingsView.playlistItemListLocal.ElementAt (0);
		//we fill the body of the view with text information for our chosen playlist (start time etc)
		fillBody ();
	}
		

	public void Dropdown_IndexChanged(int index)
	{
		string scenarioId = scenarioList.ElementAt (index);
		//we go through all playlist items within playlist chosen
		foreach(PlaylistItem p in playlistSettingsView.playlistItemListLocal){
			//if the scenario id matches then we set playlist chosen to an instance of this playlist item
			if (p.idscenario.Equals(scenarioId)) {
				playlistItemChosen = p;
				break;
			}
		}
		//we fill body agian to reflect the change in playlis items
		fillBody ();
	}

	void Update () {
		//back buton funcxtionality for android phones
		if (Input.GetKeyDown (KeyCode.Escape)) {
			SceneManager.LoadSceneAsync("VisualizationView");
		}
	}
	//Fills the body of the view with playlist ite text information
	public void fillBody()
	{
		bodyText.text = "Start Time\n\n" + playlistItemChosen.scenario.time_start +
		"\n\nEnd Time\n\n" + playlistItemChosen.scenario.time_end;
	}

	//the load button functionality which takes us to visualization view
	public void Load()
	{
		Driver.playlistDriver = playlistSettingsView;
		Driver.playlistItemChosen = playlistItemChosen;
		SceneManager.LoadScene("VisualizationView");
	}
}
